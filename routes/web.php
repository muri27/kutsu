<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\BerandaController@welcome');

Route::get('/about','App\Http\Controllers\BerandaController@about');

Route::get('/daftar-sepatu', 'App\Http\Controllers\BerandaController@daftarSepatu');

Route::get('/sepatu', 'App\Http\Controllers\SepatuController@index');

Route::get('/sepatu/create', 'App\Http\Controllers\SepatuController@create')->name('sepatu.create');

Route::post('/sepatu', 'App\Http\Controllers\SepatuController@store')->name('sepatu.store');

Route::get('/sepatu/edit/{id}', 'App\Http\Controllers\SepatuController@edit')->name('sepatu.edit');

Route::post('/sepatu/update/{id}', 'App\Http\Controllers\SepatuController@update')->name('sepatu.update');

Route::post('/sepatu/delete/{id}', 'App\Http\Controllers\SepatuController@destroy')->name('sepatu.destroy');

Route::get('/sepatu/search', 'App\Http\Controllers\SepatuController@search')->name('sepatu.search');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
