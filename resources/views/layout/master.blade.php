<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>くつ - Kutsu</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->
         <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
        <script src="{{ asset('js/jquery.js') }}"> </script>
        <script src="{{ asset('js/bootstrap-datepicker.js') }}"> </script>
        
        <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: black;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: white;
            padding: 0 20px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .tautan {
            color: white;
            margin-top: 10px;
            padding: 0 20px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
            float: right;
        }

        .links>a:hover{
            color: #cccccc;
        }

        th{
            background-color: #0c334c;
            color: white;
        }

        tr{
            background-color:#f8f9fa;
        }

        .tautan:hover{
            color: #cccccc;
            text-decoration:none;
        }

        .navigasi{
            background-color: #0c334c;
        }
        
        .card-footer {
            background: linear-gradient(
                     rgba(0,0,0, .7), 
                     rgba(0,0,0, .7)),
                     url({{asset('images/product1.png')}});
        }

        .card-header {
            background: linear-gradient(
                     rgba(0,0,0, .7), 
                     rgba(0,0,0, .7)),
                     url({{asset('images/bgfooter.jpg')}});
        }

        .kosongan{
	        height: 60px;
        }


    </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top fixed-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0 navigasi">
            <div class="fixed top-0 right-0 px-6 py-4 sm:block">
                    @if (Route::has('login'))
                        @auth
                            <a href="{{ url('/home') }}" style="margin-top:0px" class="mr-4 tautan"><img src="{{ asset('icon/person.png') }}" width="35px" /></a>
                        @else
                            <a href="{{ route('login') }}" class="mr-4 tautan">Login</a>
                            @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="mr-4 tautan">Register</a>  
                     @endif
                @endif
                @include('layout.menus')
            </div>
            @endif
        </div>

        <div class="content" >    
            @yield('content')
            <div class="card margin-container text-light" style="background-color: #0c334c">
				<div class="container mt-5">
                    <div class="col">
						<p class="card-text"><a class="nav-link text-light" href="/" style="font-size:29px">くつ</a></p>
					</div>
                </div>
                <div class="container mt-5" style="float:center;">
					<div class="row">
                        <div class="col">
							<h5 class="card-title">Menu</h5>
						</div>
						<div class="col">
							<h5 class="card-title">Contact</h5>
						</div>
					</div>
				    <div class="row">
                        <div class="col">
							<p class="card-text"><a class="nav-link text-light" href="/">Home</a></p>
						</div>
						<div class="col">
							<p class="card-text ml-4"><img src="{{ asset('icon/alamat.svg') }}" width="35">Blimbingsari C No.68J</p>
						</div>
					</div>
					<div class="row">
                        <div class="col">
							<p class="card-text"><a class="nav-link text-light" href="/daftar-sepatu">Products</a></p>
						</div>
						<div class="col">
							<p class="card-text ml-4"><img src="{{ asset('icon/email.svg') }}" width="35">muri.rifki27@gmail.com</p>
						</div>
					</div>
					<div class="row">
                        <div class="col">
							<p class="card-text"><a class="nav-link text-light" href="/about">About</a></p>
						</div>
						<div class="col">
							<p class="card-text ml-4"><img src="{{ asset('icon/phone.svg') }}" width="35">+6282144779709</p>
						</div>
					</div>
					<div class="row">
						<div class="col">
                        <br><br>
							<p class="text-center">Copyright &copy; 2020 くつ</p>
						</div>
					</div>
				</div>
            </div>
        </div>
        <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
        <script type="text/javascript">
            $('.autoplay').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1000,
            });
        </script>

        <script src="{{asset('js/main.js')}}"></script>
        <script type="text/javascript">
            $('.date').datepicker({
                format:'yyyy/mm/dd',
                autoclose: 'true'
            })
        </script>
</body>
</html>
