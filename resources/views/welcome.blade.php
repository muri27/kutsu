@extends ('layout.master')

@section ('content')
<div class="card bg-dark text-white">
  <img class="card-img" src="{{asset('images/homebg1.jpg')}}" alt="Card image">
  <div class="card-img-overlay" style="margin-top: 250px;">
    <h1 class="card-title">Discover Brand New Shoes</h1>
    <h1 class="card-title">With Reasonable Price</h1>
  </div>
</div>

<div style="margin-top:200px" class="container">
<h1>New Arrivals</h1>
<hr>
    <div class="autoplay" style=" margin-top:50px; margin-bottom:50px;">
        @foreach ($sepatu->take(5)->sortByDesc('created_at') as $data)
                <div><img src="{{ $data->foto != null ? asset('images/'.$data->foto) : asset('images/image-not-found.jpg') }}" style="width:250px; height:250px"></div>
        @endforeach
    </div>              

</div>
<a href="/daftar-sepatu" class="btn btn-info btn-lg" style="margin-bottom:100px;">See More</a>

<div class="card-footer text-light mt-5" style="margin-bottom:100px;">
    <div class="container">
        <div class="row" style="margin-top:50px">
            <div class="col">
                <h1 class="card-title">{{ $sepatu->count()}}</h1>
            </div>

            <div class="col">
                <h1 class="card-title">2</h1>
            </div>

            <div class="col">
                <h1 class="card-title">150</h1>
            </div>
        </div>

        <div class="row" style="margin-bottom:50px">
            <div class="col">
                <p class="card-text text-light">Products</p>
            </div>
            <div class="col">
                <p class="card-text text-light">Users</p>
            </div>
            <div class="col">
                <p class="card-text text-light">Visited</p>
            </div>
        </div>
    </div>
</div>  

<div style="margin-top:20px; margin-bottom:100px;" class="container">
<h1>Top Products</h1>
<hr>
      <div class="card-deck">
      @foreach ($sepatu->take(3)->sortByDesc('harga') as $data)
				<div class="card" style="margin-top:50px; align-items:center;">
                <img src="{{ $data->foto != null ? asset('images/'.$data->foto) : asset('images/image-not-found.jpg') }}"
                style="width:250px; height:250px; margin-top:20px">
					<div class="card-body">
						<h5 class="card-title" style="font-weight: bold">{{ $data->nama }}</h5>
						<p class="card-text">{{ "Rp ".number_format($data->harga,2,',','.') }}</p>
						<a href="/daftar-sepatu"class="btn btn-outline-info">Go To Products</a>
					</div>
				</div>
     @endforeach
     </div>
</div>


			

@endsection