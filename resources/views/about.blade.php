@extends ('layout.master')

@section ('content')

<div class="container mt-5" style="margin-bottom:100px;">
    <div class="row">
        <div class="col mt-5">
            <h3 class="text-black-50">
                Hello!
            </h3>
            <h2 class="text-dark">
                Saya Muhammad Rifki
            </h2>

            <p class="lead">くつ (read: kutsu) merupakan sebuah situs web yang diperuntukkan untuk tugas akhir
            dari mata kuliah Praktikum Pemrograman Web II yang diampu Pak Dinar. Situs ini menerapkan semua materi yang
            telah disampaikan di kelas sebelumnya. Situs web ini berbasis Laravel dengan menggunakan beberapa template
            seperti Bootstrap, COREUI, dan Slick.
            </p>

            <button class="btn btn-outline-dark">Contact Me</button>
        </div>

        <div class="col" id="position-profil">
            <img src="{{ asset('images/profil.jpg') }}" width="500" height="430">
        </div>
    </div>

</div>
@endsection