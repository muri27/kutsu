@extends('layout.master')

@section('content')
<section class="card-header text-light">
<br><br><br><br><br>
      	<div class="container" style="margin-top:100px;">
          <h1>List Of Our Products</h1>
				</div>
</section>
<section id="sepatu" class="py-20 ">
    <div class="container">
      <div class="row row-cols-1 row-cols-md-3">
      @foreach ($sepatu->sortBy('nama') as $data)
      <div class="col mb-4">
				<div class="card h-100" style="margin-top:50px; align-items:center;">
                <img src="{{ $data->foto != null ? asset('images/'.$data->foto) : asset('images/image-not-found.jpg') }}"
                style="width:250px; height:250px; margin-top:20px">
					<div class="card-body" style=>
						<h5 class="card-title" style="font-weight: bold;">{{ $data->nama }}</h5>
            <h6 class="card-title" style="color:gray;">{{ $data->vendor }}</h6>
            <h6 class="card-title">{{ $data->tahun->format('Y') }}</h6>
						<p class="card-text">{{ "Rp ".number_format($data->harga,2,',','.') }}</p>
					</div>
				</div>
      </div>
     @endforeach
     </div>
    </div>
  </section>
  <br><br><br>
@endsection