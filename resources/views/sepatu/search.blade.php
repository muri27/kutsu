@extends('layout.master')
@section('content')
<div class="kosongan"></div>
<div class="container">
    @if(count($data_sepatu))
    <div class="kosongan"></div>
        <div class="alert alert-success"> Found <strong>{{ count($data_sepatu) }}</strong> Data With <strong>{{ $cari }}</strong>
        </div>
		@if(Session::has('pesan'))
        <div class="kosongan"></div>
        <div class="alert alert-success">{{Session::get('pesan')}}</div>
  		@elseif(Session::has('pesanHapus'))
          <div class="kosongan"></div>
			<div class="alert alert-danger">{{Session::get('pesanHapus')}}</div>
		@endif
        <div style="margin-top:50px">
            <h2 style="float:left">Shoes Data</h2>
            <br><br>
            <div>
		    <a href="{{ route('sepatu.create') }}" class="btn btn-primary" style="float:left; margin-top:10px;">
			    Add Shoes
		    </a>
		    <form action="{{ route('sepatu.search') }}" method="get">
		        @csrf
        	    <input type="text" name="kata" class="form-control" placeholder="Search..."
			    style="width:30%; display:inline; margin-top:10px; margin-bottom:10px; float:right;">
    	    </form>
	        </div>
	    </div>
        <table class="table">
            <thead>
                <tr>
                <th>No</th>
				<th>Type</th>
				<th>Brand</th>
				<th>Photo</th>
				<th>Price</th>
				<th>Year</th>
				<th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_sepatu as $sepatu)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $sepatu->nama }}</td>
                        <td>{{ $sepatu->vendor }}</td>
                        <td><img src="{{ $sepatu->foto != null ? asset('images/'.$sepatu->foto) : asset('images/image-not-found.jpg') }}"
                        style="width: 100px"></td>
                        <td>{{ "Rp ".number_format($sepatu->harga,2,',','.') }}</td>
                        <td>{{ $sepatu->tahun->format('Y') }}</td>
                        <td>
                            <form action="{{ route('sepatu.destroy',$sepatu->id) }}" method="post">
                                @csrf
                                <a href="{{ route('sepatu.edit',$sepatu->id) }}" class="btn btn-info">Edit</a>
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are You Sure You Want to Delete?')">Delete</button> 
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div style="margin-bottom:80px;">
            <div class="kiri"><strong>Total Shoes: {{ $jumlah_sepatu }}</strong></div>
            <div class="kanan">{{ $data_sepatu->links() }}</div>
        </div>
    @else
    <div class="kosongan"></div>
        <div style="margin-bottom:75px">
            <h4> Data {{ $cari }} Not Found </h4>
        </div>
    @endif
</div>
@endsection
