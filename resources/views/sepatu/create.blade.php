@extends ('layout.master')
@section ('content')
<div class="kosongan"></div>
<div class="container" style>
	<div class=card style="margin-top:50px; margin-bottom:50px; background-color:#f8f9fa">
	<h4 style="margin:20px 0px">Add Shoes</h4>
	<form method="post" action="{{ route('sepatu.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="form-group row" style="margin-right:20px;">
			<label form="nama_sepatu" class="col-sm-3 col-form-label">Type</label>
			<div class="col-sm-9">
				<input type="text" id="nama" name="nama" class="form-control">
				@error('nama')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="vendor" class="col-sm-3 col-form-label">Brand</label>
			<div class="col-sm-9">
				<input type="text" id="vendor" name="vendor" class="form-control">
				@error('vendor')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu" class="col-sm-3 col-form-label">Photo</label>
			<div class="col-sm-9">
			<input type="file" class="form-control-file" name="foto">
				@error('foto')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu"class="col-sm-3 col-form-label">Price</label>
			<div class="col-sm-9">
				<input type="text" id="harga" name="harga" class="form-control">
				@error('harga')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu"class="col-sm-3 col-form-label">Year</label>
			<div class="col-sm-9">
				<input type="text" id="tahun" name="tahun" class="date form-control">
				@error('tahun')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<div class="col-sm-9">
				<button type="submit" class="btn btn-outline-primary">Save</button>
				<a class="btn btn-outline-danger" href="/sepatu">Cancel</a>
			</div>
		</div>
	</div>
</div>
@endsection