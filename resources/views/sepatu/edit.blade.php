@extends ('layout.master')
@section ('content')
<div class="kosongan"></div>
<div class="container">
	<div class=card style="margin-top:50px; margin-bottom:50px; background-color:#f8f9fa">
	<h4 style="margin:20px 0px">Edit</h4>
	<form method="post" action="{{ route('sepatu.update', $sepatu->id) }}" enctype="multipart/form-data">
		@csrf
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu" class="col-sm-3 col-form-label">Type</label>
			<div class="col-sm-9">
				<input type="text" id="nama" name="nama" class="form-control" value="{{ $sepatu->nama }}">
				@error('nama')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu" class="col-sm-3 col-form-label">Brand</label>
			<div class="col-sm-9">
				<input type="text" id="vendor" name="vendor" class="form-control" value="{{ $sepatu->vendor }}">
				@error('vendor')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu" class="col-sm-3 col-form-label">Photo</label>
			<div class="col-sm-9">
			@if($sepatu->foto != null)
			<img src="{{ asset('images/'.$sepatu->foto) }}" style="width: 250px">
			@else
			<img src="{{ asset('images/image-not-found.jpg') }}" style="width: 250px; margin-bottom:10px;">
			<br>
			<label>*) Don't fill it if you don't want to change.</label>
			@endif
			<input type="file" class="form-control-file" name="foto">
				@error('foto')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu"class="col-sm-3 col-form-label">Price</label>
			<div class="col-sm-9">
				<input type="text" id="harga" name="harga" class="form-control" value="{{ $sepatu->harga }}">
				@error('harga')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<label form="sepatu"class="col-sm-3 col-form-label">Year</label>
			<div class="col-sm-9">
				<input type="text" id="tahun" name="tahun" class="date form-control" value="{{ $sepatu->tahun->format('Y/m/d') }}">
				@error('tahun')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
		</div>
		<div class="form-group row" style="margin-right:20px;">
			<div class="col-sm-9">
				<button type="submit" class="btn btn-success">Update</button>
				<a style="color: white;"class="btn btn-warning" href="/sepatu">Batal</a>
			</div>
		</div>
		</div>
</div>
@endsection