<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sepatu;

class BerandaController extends Controller
{
    //fungsi welcome
    public function welcome(){
        $sepatu = Sepatu::all();
        return view('welcome', compact('sepatu'));
    }

    //fungsi about
    public function about(){
    	return view('about');
    }

    //fungsi daftar-sepatu
    public function daftarSepatu(){
        $sepatu = Sepatu::all();
        return view('sepatu', compact('sepatu'));
    }
}
