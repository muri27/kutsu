<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sepatu;
use File;


class SepatuController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }

    public function index(){
        $batas = 3;
        $jumlah_sepatu = Sepatu::count();
        $data_sepatu = Sepatu::orderBy('harga','desc')->paginate($batas);
        $no = $batas *($data_sepatu->currentPage()-1);

        return view('sepatu.index', compact('data_sepatu','no','jumlah_sepatu'));
    }

    public function search(Request $request){
        $batas = 3;
        $cari = $request->kata;
        $data_sepatu = Sepatu::where('nama','like',"%".$cari."%")->orwhere('vendor','like',"%".$cari."%")->orderBy('harga','desc')
        ->paginate($batas);
        $jumlah_sepatu = $data_sepatu->count();
        $no = $batas *($data_sepatu->currentPage()-1);

        return view('sepatu.search', compact('data_sepatu','no','jumlah_sepatu','cari'));
    }

    public function create(){
    	return view('sepatu.create');
    }

    public function store(Request $request){
        $this->validate($request,[
            'nama' => 'required|string|max:50',
            'vendor' => 'required|string',
            'foto' => 'required|image|mimes:jpeg,jpg,png',
            'harga' => 'required|numeric',
            'tahun' => 'required|date'
        ]);

        $foto = $request->foto;
        $namafile = time().'.'.$foto->getClientOriginalExtension();
        $foto->move('images/', $namafile);

    	$sepatu = new Sepatu;
    	$sepatu->nama = $request->nama;
    	$sepatu->vendor = $request->vendor;
        $sepatu->foto = $namafile;
        $sepatu->harga = $request->harga;
    	$sepatu->tahun = $request->tahun;
    	$sepatu->save();

    	return redirect('/sepatu')->with('pesan','Data Sepatu Berhasil Ditambahkan');
    }

    public function destroy($id){
        $sepatu = Sepatu::find($id);
        $namafile = $sepatu->foto;
        File::delete('images/'.$namafile);
        $sepatu->delete();

        return redirect('/sepatu')->with('pesanHapus','Data Sepatu Berhasil Dihapus');;
    }

    public function edit($id){
        $sepatu = Sepatu::find($id);

        return view('sepatu.edit', compact('sepatu'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'nama' => 'string|max:50',
            'vendor' => 'string',
            'foto' => 'image|mimes:jpeg,jpg,png',
            'harga' => 'numeric',
            'tahun' => 'date'
        ]);

        $sepatu = Sepatu::find($id);
        $sepatu->nama = request('nama');
        $sepatu->vendor = request('vendor');
        $sepatu->harga = request('harga');
        $sepatu->tahun = request('tahun');
        if (request('foto')!= null){
            File::delete('images/'.$sepatu->foto);

            $foto = request('foto');
            $namafile = time().'.'.$foto->getClientOriginalExtension();
            $foto->move('images/', $namafile);
            $sepatu->foto = $namafile;
        }
        $sepatu-> save();

        return redirect('/sepatu')->with('pesan','Data Sepatu Berhasil Diubah');;
    }
}
